%{
    #include <stdio.h>
    #include <string.h>
    #include <stdbool.h>
    #include "y.tab.h"
    char c[]= "_";
    char *string_copy(const char *s);
%}

/* esto es para poder contar las líneas de código*/
%option yylineno

char [A-Za-z]
digit [0-9]
comparator (<|>)=?|==|!=
double [0-9]+\.[0-9]+

%%


"#"(.*)                       /* Comentario de una sola línea */
[\t \n \r]                    /* Ignoramos espacios */
                    
"**"                          {yylval.string=string_copy(yytext); return POW;}

"AND"                         return AND;
"OR"                          return OR;

"IF"                          return IF;
"THEN"                        return THEN;

"PRINT"                       return PRINT;
"WHILE"                       return WHILE;
"DO"                          return DO;
"END"                         return END;


"FINISH"                      return FIN;
"START"                       return START;
"READ"                        return READ;
"DRAW"                        return DRAW;
"FIGURE"                      return FIGURE;
"RANDOM"                      return RANDOM;
"LAYOUT"                      return LAYOUT;


"SIN"                         return SIN;
"COS"                         return COS;
"ARCCOS"                      return ARCCOS;
"TAN"                         return TAN;
"ARCSIN"                      return ARCSIN;
"ARCTAN"                      return ARCTAN;
"LOG"                         return LOG;

"EVAL"                        return EVAL;  

"CONST"                       return CONST;
"TRACE"                       return TRACE; 
"RED"                         {yylval.string=string_copy(yytext);return RED;}
"BLUE"                        {yylval.string=string_copy(yytext);return BLUE;}
"GREEN"                       {yylval.string=string_copy(yytext);return GREEN;}
"YELLOW"                      {yylval.string=string_copy(yytext);return YELLOW;}
"'"(.)*"'"                    {yylval.string=string_copy(yytext);return STRING;}
{comparator}                  {yylval.string=string_copy(yytext);return COMPARATOR;}
{char}({char}|{digit}|"_")*   {yylval.string=string_copy(yytext);return VARNAME;}
-?{digit}+                    {yylval.decimal=atof(yytext); return DOUBLE;}
-?{double}                    {yylval.decimal=atof(yytext); return DOUBLE;}

.                             return yytext[0];

%%

char *string_copy(const char *s) {
    char *const result = malloc(strlen(s) + 1);
    if (result != NULL) strcpy(result, s);
    return result;
}