# MateCompiler

### Compilation

```sh
$ make
```

### Prerequesites
**Python3** is needed in order to run the project correctly.

Also are the following libraries:

[numpy](https://numpy.org/install/)
[plotly](https://plotly.com/python/getting-started/)

### Installation:
```sh
$pip3 install numpy
$pip3 install plotly
```
### Running the compiler
```sh
$ ./compiler < Examples/example.mate
$ python3 out.py
```