%{

/*
 *  Librerias a utilizar en el proyecto
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <math.h>

/*
 * Libreria (no propia) que nos permite el uso de un hashmap
 * seguro y eficiente con manejo muy bueno de colisiones.
 * Repo del git:
 * https://github.com/DavidLeeds/hashmap 
 */
#include "./Utils/hashmap.h"

/*
 *  Variables Globales y defines
 */
extern char *yytext;
#define YYDEBUG_LEXER_TEXT yytext
int tabs=0;
int yydebug=1;
int myflag = 0;

/*
 *  Definicion de funciones
 */
int yylex();
void yylex_destroy();
void yyerror(char ** result, const char *s);
char * concatenate_code(char*string1, char*string2);
char *string_copy(const char *s);
void addTabs(char * s, int n);
char * declareVar(char * v, char * op, int flag);
char * checkIfVarExists(char * v);
char * remove_last_char(char * str);
void checkIfConst(char*v);
double checkZero(double d);
/*
 *  Primeros pasos para comenzar a usar el hashmap
 */

/* Estrucura de datos a guardar */
struct variable {
    
    /* Nombre de la variable, key del hashmap */
    char * name;

    /*
    *  Dato que nos permite saber si la variable
    *  es una constante, funciona como un flag.
    */
    char is_cte;

};

/* 
 * Macro que nos permite definir los tipos de datos
 * que usaremos para la clave del hashmap y la estructura
 * que representara a los datos en si.
 */
HASHMAP(char, struct variable) map;

/* Variables globales para poder trabajar con el hashmap */
const char * name;
struct variable * var;
void * temp;

%}

/* Tipos de datos a utilizar */
%union
{
    int integer;
    float decimal;
    char *string;
    char character;
}

%expect 20

/* Tokens de la gramatica */
%token EVAL POW  TRACE SIN LOG ARCTAN COS ARCCOS ARCSIN TAN AND RANDOM OR LAYOUT FIGURE DRAW READ RED GREEN BLUE YELLOW  IF THEN  WHILE DO END  PRINT  COMPARATOR VARNAME  DOUBLE FIN START STRING CONST

/* 
 * Parametro adicional pasado a la funcion yyparse que
 * sirve como recipiente para volcar el codigo python que
 * generemos.
 */
%parse-param {char ** code_result}

/* Definimos la precedencia de los operadores que usemos */
%left '+' '~' 
%left POW '*' '/' '%' COMPARATOR SIN COS TAN ARCSIN ARCCOS ARCTAN LOG
%left '(' ')'

/* Indicamos que estado inicial es S */
%start S

%%

/* 
 * S --> estado inicial: consiste en definir los delimitadores START y END
 * para indicar donde arranca y termina el codigo.
 */
S               : START CODE FIN {*code_result = (char*) malloc(strlen($<string>2) + 2);sprintf(*code_result, "%s\n", $<string>2); free($<string>2);}       
                | START FIN  {*code_result = NULL;}

/* 
 * CODE --> estado recursivo: en este estado es donde la persona que utilice
 * nuestro lenguaje escribira todo el codigo a ejecutar. Incluye:
 *   - Declaracion de variables.
 *   - Asignaciones.
 *   - Estructura condicional IF.
 *   - Estructura iterativa WHILE hasta cumplir condicion.
 *   - Funcion PRINT.
 *   - Funcion READ para leer de entrada estandar.
 *   - Directivas para realizar plots de funciones.
 */
CODE            : DECLARATION CODE {$<string>$ = concatenate_code($<string>1, $<string>2); addTabs($<string>$, tabs); }
                | DECLARATION   {$<string>$ = concatenate_code($<string>1, NULL); addTabs($<string>$, tabs);}
                | CONDITIONAL CODE   {$<string>$ = concatenate_code($<string>1, $<string>2); addTabs($<string>$, tabs); }
                | CONDITIONAL  {$<string>$ = concatenate_code($<string>1, NULL);addTabs($<string>$, tabs); }
                | WHILE_LOOP CODE {$<string>$ = concatenate_code($<string>1, $<string>2); addTabs($<string>$, tabs); }
                | WHILE_LOOP {$<string>$ = concatenate_code($<string>1, NULL);addTabs($<string>$, tabs); }
                | ASSIGNATION {$<string>$ = concatenate_code($<string>1, NULL);addTabs($<string>$, tabs); }
                | ASSIGNATION CODE  {$<string>$ = concatenate_code($<string>1, $<string>2); addTabs($<string>$, tabs); }
                | PRINT_OP CODE {$<string>$ = concatenate_code($<string>1, $<string>2); addTabs($<string>$, tabs); }
                | PRINT_OP {$<string>$ = concatenate_code($<string>1, NULL);addTabs($<string>$, tabs); }
                | READ_CHAR CODE {$<string>$ = concatenate_code($<string>1, $<string>2); addTabs($<string>$, tabs); }
                | READ_CHAR {$<string>$ = concatenate_code($<string>1, NULL);addTabs($<string>$, tabs); }
                | SET_LAYOUT CODE {$<string>$ = concatenate_code($<string>1, $<string>2); addTabs($<string>$, tabs); }
                | SET_LAYOUT {$<string>$ = concatenate_code($<string>1, NULL);addTabs($<string>$, tabs); }
                | DRAW_FIGURE CODE {$<string>$ = concatenate_code($<string>1, $<string>2); addTabs($<string>$, tabs); }
                | DRAW_FIGURE {$<string>$ = concatenate_code($<string>1, NULL);addTabs($<string>$, tabs); }
                | ADD_TRACE CODE {$<string>$ = concatenate_code($<string>1, $<string>2); addTabs($<string>$, tabs); }
                | ADD_TRACE {$<string>$ = concatenate_code($<string>1, NULL);addTabs($<string>$, tabs); }


/*
 * Estado "auxiliar" que permite realizar optimizaciones
 * de codigo para numeros consecutivos, por ejemplo la
 * sentencia: 
 *       "X = Y + 6 + 7;" queda como "X = Y + 13;"  
 */
REDUCED_EXP     : DOUBLE {$<decimal>$ = $<decimal>1;}
                | REDUCED_EXP '+' REDUCED_EXP {$<decimal>$ = $<decimal>1 + $<decimal>3;}
                | REDUCED_EXP '~'  REDUCED_EXP {$<decimal>$ = $<decimal>1 - $<decimal>3;}
                | REDUCED_EXP '*' REDUCED_EXP {$<decimal>$ = $<decimal>1 * $<decimal>3;}
                | REDUCED_EXP '/' REDUCED_EXP {$<decimal>$ = $<decimal>1 / checkZero($<decimal>3);}
                | REDUCED_EXP '%' REDUCED_EXP {$<integer>$ = (int)$<decimal>1 % (int)$<decimal>3;}
                | REDUCED_EXP POW REDUCED_EXP {$<decimal>$ = pow($<decimal>1, $<decimal>3);}
                | '(' REDUCED_EXP ')' {$<decimal>$ = $<decimal>2;}
              


/*
 * Estado donde se define el string que contendra el nombre de la variable
 * se chequea que la variable no este repetida consultado al hashmap. Al mismo
 * tiempo se le agrega un '_' al final para evitar problemas con el nombre de 
 * las variables ya que si un usuario quiere nombrar a su variable como "def" 
 * habria un conflicto con la directiva "def" de Python.
 */
VARIABLE        : VARNAME {strcat($<string>1,"_");$<string>$ = $<string>1;checkIfVarExists($<string>1);} 



/*
 * Estado creado con el mismo proposito que VARIABLE
 * pero con el fin de evitar conflictos en la gramática.
 */
VARIABLE_DECLARATION : VARNAME {strcat($<string>1,"_"); $<string>$ = $<string>1;}




/*
 * Estado donde se definen expresiones matemáticas en conjunto con
 * todas las funciones matematicas aplicables de nuestro lenguaje
 * siendo estas:
 *   - SIN: calcula el seno.
 *   - COS: calcula el coseno.
 *   - TAN: calcula la tangente.
 *   - ARCSIN: calcula el arcosneo.
 *   - ARCCOS: calcula el arcocoseno.
 *   - ARCTAN: calcula el arcotangente
 *   - LOG: calcula un logaritmo en base n de un numero m.
 *   - EVALUATE: permite evaluar funciones complejas, explicada mas abajo.
 */
OPERATION       :  ECUATION {$<string>$ = $<string>1;}
                | '(' OPERATION ')' {$<string>$ = malloc (3 + strlen($<string>2)); sprintf($<string>$, "(%s)", $<string>2);}
                | OPERATION '+' OPERATION {$<string>$ = malloc(  5 + strlen($<string>1) + strlen($<string>3)); sprintf($<string>$, "%s + %s", $<string>1, $<string>3); }
                | OPERATION '~' OPERATION {$<string>$ = malloc(  5 + strlen($<string>1) + strlen($<string>3)); sprintf($<string>$, "%s - %s", $<string>1, $<string>3); }
                | OPERATION '*' OPERATION {$<string>$ = malloc(  5 + strlen($<string>1) + strlen($<string>3)); sprintf($<string>$, "%s * %s", $<string>1, $<string>3); }
                | OPERATION '/' OPERATION {$<string>$ = malloc(  5 + strlen($<string>1) + strlen($<string>3)); sprintf($<string>$, "%s / %s", $<string>1, $<string>3); }
                | OPERATION '%' OPERATION {$<string>$ = malloc(  5 + strlen($<string>1) + strlen($<string>3)); sprintf($<string>$, "%s %% %s", $<string>1, $<string>3); }
                | OPERATION POW OPERATION {$<string>$ = malloc(  5 + strlen($<string>1) + strlen($<string>3)); sprintf($<string>$, "%s %s %s", $<string>1, $<string>2, $<string>3); }
                | RANDOM {$<string>$ = malloc(21); sprintf($<string>$, "np.random.randn(N)");}
                | SIN OPERATION {$<string>$ = malloc(strlen($<string>2 + 10)); sprintf($<string>$, "np.sin(%s)", $<string>2);}
                | COS OPERATION {$<string>$ = malloc(strlen($<string>2 + 10)); sprintf($<string>$, "np.cos(%s)", $<string>2);}
                | TAN OPERATION {$<string>$ = malloc(strlen($<string>2 + 10)); sprintf($<string>$, "np.tan(%s)", $<string>2);}
                | ARCSIN OPERATION {$<string>$ = malloc(strlen($<string>2 + 15)); sprintf($<string>$, "np.arcsin(%s)", $<string>2);}
                | ARCCOS OPERATION {$<string>$ = malloc(strlen($<string>2 + 15)); sprintf($<string>$, "np.arccos(%s)", $<string>2);}
                | ARCTAN OPERATION {$<string>$ = malloc(strlen($<string>2 + 15)); sprintf($<string>$, "np.arctan(%s)", $<string>2);}
                | LOG OPERATION OPERATION {$<string>$ = malloc(strlen($<string>2) + strlen($<string>3) + 15); sprintf($<string>$, "np.log10(%s)/np.log10(%s)", $<string>2, $<string>3);}
                | EVALUATE  {$<string>$ = malloc(strlen($<string>1 +1)); sprintf($<string>$, "%s", $<string>1);}

/*
 * Estado donde se definen expresiones matematicas en conjunto
 * con variables. Donde se optimizan valores conjuntos de valores.
 */
ECUATION        : VARIABLE '+' REDUCED_EXP {$<string>$ = malloc(  5 + strlen($<string>1) +100); sprintf($<string>$, "%s + %f", $<string>1, $<decimal>3); }
                | REDUCED_EXP '+' VARIABLE {$<string>$ = malloc(  5 + 100 + strlen($<string>3)); sprintf($<string>$, "%f + %s", $<decimal>1, $<string>3); }
                | VARIABLE '+' VARIABLE {$<string>$ = malloc(  5 + strlen($<string>1) + strlen($<string>3)); sprintf($<string>$, "%s + %s", $<string>1, $<string>3); }
                | VARIABLE '~' VARIABLE {$<string>$ = malloc(  5 + strlen($<string>1) + strlen($<string>3)); sprintf($<string>$, "%s - %s", $<string>1, $<string>3); }
                | VARIABLE '*' VARIABLE {$<string>$ = malloc(  5 + strlen($<string>1) + strlen($<string>3)); sprintf($<string>$, "%s * %s", $<string>1, $<string>3); }
                | VARIABLE '/' VARIABLE {$<string>$ = malloc(  5 + strlen($<string>1) + strlen($<string>3)); sprintf($<string>$, "%s / %s", $<string>1, $<string>3); }
                | VARIABLE '%' VARIABLE {$<string>$ = malloc(  5 + strlen($<string>1) + strlen($<string>3)); sprintf($<string>$, "%s %% %s", $<string>1, $<string>3); }
                | VARIABLE POW VARIABLE {$<string>$ = malloc(  6 + strlen($<string>1) + strlen($<string>3)); sprintf($<string>$, "%s %s %s", $<string>1, $<string>2, $<string>3); }
                | REDUCED_EXP '+' PAR_ECUA {$<string>$ = malloc(  5 + strlen($<string>3) + 100); sprintf($<string>$, "%f + %s", $<decimal>1, $<string>3); }
                | REDUCED_EXP '~' PAR_ECUA {$<string>$ = malloc(  5 + strlen($<string>3) + 100); sprintf($<string>$, "%f - %s", $<decimal>1, $<string>3); }
                | REDUCED_EXP '*' PAR_ECUA {$<string>$ = malloc(  5 + strlen($<string>3) + 100); sprintf($<string>$, "%f * %s", $<decimal>1, $<string>3); }
                | REDUCED_EXP '/' PAR_ECUA {$<string>$ = malloc(  5 + strlen($<string>3) + 100); sprintf($<string>$, "%f / %s", $<decimal>1, $<string>3); }
                | REDUCED_EXP '%' PAR_ECUA {$<string>$ = malloc(  5 + strlen($<string>3) + 100); sprintf($<string>$, "%f %% %s", $<decimal>1, $<string>3); }
                | REDUCED_EXP POW PAR_ECUA {$<string>$ = malloc(  6 + strlen($<string>3) + 100); sprintf($<string>$, "%f %s %s", $<decimal>1, $<string>2, $<string>3); }
                | VARIABLE '~' REDUCED_EXP {$<string>$ = malloc(  5 + strlen($<string>1) + 100); sprintf($<string>$, "%s - %f", $<string>1, $<decimal>3); }
                | REDUCED_EXP '~' VARIABLE {$<string>$ = malloc(  5 + 100 + strlen($<string>3)); sprintf($<string>$, "%f - %s", $<decimal>1, $<string>3); }
                | VARIABLE '*' REDUCED_EXP {$<string>$ = malloc(  5 + strlen($<string>1) + 100); sprintf($<string>$, "%s * %f", $<string>1, $<decimal>3); }
                | REDUCED_EXP '*' VARIABLE {$<string>$ = malloc(  5 + 100 + strlen($<string>3)); sprintf($<string>$, "%f * %s", $<decimal>1, $<string>3); }
                | VARIABLE '/' REDUCED_EXP {$<string>$ = malloc(  5 + strlen($<string>1) +100); sprintf($<string>$, "%s / %f", $<string>1, $<decimal>3); }
                | REDUCED_EXP '/' VARIABLE {$<string>$ = malloc(  5 + 100 + strlen($<string>3)); sprintf($<string>$, "%f / %s", $<decimal>1, $<string>3); }
                | VARIABLE '%' REDUCED_EXP {$<string>$ = malloc(  5 + strlen($<string>1) + 100); sprintf($<string>$, "%s %% %f", $<string>1, $<decimal>3); }
                | REDUCED_EXP '%' VARIABLE {$<string>$ = malloc(  5 + 100 + strlen($<string>3)); sprintf($<string>$, "%f %% %s", $<decimal>1, $<string>3); }
                | VARIABLE POW REDUCED_EXP {$<string>$ = malloc(  5 + strlen($<string>1) + 100); sprintf($<string>$, "%s ** %f", $<string>1, $<decimal>3); }
                | REDUCED_EXP POW VARIABLE  {$<string>$ = malloc(  5 + 100 + strlen($<string>3)); sprintf($<string>$, "%f ** %s", $<decimal>1, $<string>3); }
                | VARIABLE  {$<string>$ = $<string>1;} 
                | REDUCED_EXP {$<string>$ = malloc(100 + 1); sprintf($<string>$, "%f", $<decimal>1);}
                | PAR_ECUA 
              
/* Estado "auxiliar" que nos permite implementar parentesis evitando ambigüedad */
PAR_ECUA        : '(' ECUATION ')' {$<string>$ = malloc (3 + strlen($<string>2)); sprintf($<string>$, "(%s)", $<string>2);}

/* 
 * Estado que nos permite definir variables en la forma:
 * <name> = [<value>|<operation>|<array>|<figure>];
 * Las variables son registradas en el hashmap para realizar
 * manejos de errores mas adelante que involucren el uso de 
 * variables no definidas.
 */
DECLARATION     : VARIABLE_DECLARATION  '=' OPERATION ';' { checkIfConst($<string>1);$<string>$ = declareVar($<string>1, $<string>3,0);}
                | VARIABLE_DECLARATION ';'  { checkIfConst($<string>1);$<string>$ = declareVar($<string>1, NULL,0);}
                | VARIABLE_DECLARATION '=' ARRAY ';' { checkIfConst($<string>1);$<string>$ = declareVar($<string>1, $<string>3,0);}
                | VARIABLE_DECLARATION '=' FIGURE_VAL ';'{ checkIfConst($<string>1);$<string>$ = declareVar($<string>1, $<string>3,0);}
                | CONST VARIABLE_DECLARATION '=' REDUCED_EXP ';' { checkIfConst($<string>2); char*aux = malloc(100 + 1); sprintf(aux, "%f", $<decimal>4); $<string>$ = declareVar($<string>2, aux,1);}
/*
 * Estado donde se realizan asignaciones sobre variables.
 */
ASSIGNATION     : VARIABLE '+' '+' ';' {checkIfConst($<string>1); $<string>$ = malloc(2 * strlen($<string>1) + 8);sprintf($<string>$, "%s = %s + 1", $<string>1, $<string>1); free($<string>1);}
                | VARIABLE '~' '~' ';' {checkIfConst($<string>1);$<string>$ = malloc(2 * strlen($<string>1) + 8);sprintf($<string>$, "%s = %s - 1", $<string>1, $<string>1); free($<string>1);}
                | VARIABLE '*' '=' OPERATION ';' {checkIfConst($<string>1);$<string>$ = malloc(2 * strlen($<string>1) + strlen($<string>4) + 9);sprintf($<string>$, "%s = %s * (%s)", $<string>1, $<string>1, $<string>4); free($<string>1); free($<string>4);}
                | VARIABLE '/' '=' OPERATION ';' {checkIfConst($<string>1);$<string>$ = malloc(2 * strlen($<string>1) + strlen($<string>4) + 9);sprintf($<string>$, "%s = %s / (%s)", $<string>1, $<string>1, $<string>4); free($<string>1); free($<string>4);}
                | VARIABLE '+' '=' OPERATION ';' {checkIfConst($<string>1);$<string>$ = malloc(2 * strlen($<string>1) + strlen($<string>4) + 9);sprintf($<string>$, "%s = %s + (%s)", $<string>1, $<string>1, $<string>4); free($<string>1); free($<string>4);}
                | VARIABLE '~' '=' OPERATION ';' {checkIfConst($<string>1);$<string>$ = malloc(2 * strlen($<string>1) + strlen($<string>4) + 9);sprintf($<string>$, "%s = %s * (%s)", $<string>1, $<string>1, $<string>4); free($<string>1); free($<string>4);}
/*
 * Estado donde se definen arrays con el formato [ <lista de valores> ]
 */
ARRAY           : '[' LIST ']' {$<string>$ = malloc(strlen($<string>2) + 3); sprintf($<string>$, "[%s]", $<string>2); free($<string>2);}

/*
 * Lista de valores para los array.
 */
LIST            : OPERATION ',' LIST {$<string>$ = malloc(100 + strlen($<string>3) + 3); sprintf($<string>$, "%s, %s", $<string>1, $<string>3);   free($<string>3);}
                | OPERATION {$<string>$ = $<string>1;}
                | ARRAY ',' LIST {$<string>$ = malloc(100 + strlen($<string>3) + 3); sprintf($<string>$, "%s, %s", $<string>1, $<string>3);   free($<string>3);}
                | ARRAY {$<string>$ = $<string>1;}
                | STRING ',' LIST  {$<string>$ = malloc(strlen($<string>1) + strlen($<string>3) + 3); sprintf($<string>$, "%s, %s", $<string>1, $<string>3);   free($<string>3);}
                | STRING {$<string>$ = $<string>1;}

/* 
 * Funcion de print, ejemplo: 
 * PRINT "HOLA"; --> muestra hola por pantalla.
 */
PRINT_OP        : PRINT OPERATION ';' {$<string>$ = malloc(8 + strlen($<string>2)); sprintf($<string>$, "print(%s)", $<string>2);}
                | PRINT STRING ';' {$<string>$ = malloc(10 + strlen($<string>2)); sprintf($<string>$, "print(%s)", $<string>2);}
/*
 * Funcion READ que lee por entrada estandar
 */                                                                                             
READ_CHAR       : READ VARIABLE ';' {$<string>$ = malloc(1008); sprintf($<string>$, "\n%s = \"\"\nwhile True:\n\tc = sys.stdin.read(1)\n\tif c == '\\n':\n\t\tbreak\n\t%s += c\n%s = float(%s)", $<string>2, $<string>2, $<string>2, $<string>2);}



/*
 * Estados donde se definen la estructura condicional IF y la
 * estructura ciclica WHILE la cual se hara hasta que se cumpla
 * una condicion especifica.
 */
CONDITIONAL     : IF '(' CONDITION ')' OPEN CODE CLOSE {$<string>$ = malloc(strlen($<string>3)+ strlen($<string>6) + tabs+ 10); sprintf($<string>$, "if %s:\n%s", $<string>3, $<string>6);}
WHILE_LOOP      : WHILE '(' CONDITION ')' OPEN CODE CLOSE  {$<string>$ = malloc(strlen($<string>3)+ strlen($<string>6) + tabs+ 10); sprintf($<string>$, "while %s:\n%s", $<string>3, $<string>6);}

/*
 * Delimitadores de las estructuras mencionadas arriba de este comentario
 * la variable "tabs" se usa para generar tabs en el codigo python de salida
 * dentro del IF y el WHILE ya que en python los bloques de codigo se determinan
 * segun su tabulacion.
 *
 * OPEN agrega tabs y CLOSE los quita.
 */
OPEN            : THEN {tabs++;}
                | DO   {tabs++;}
CLOSE           : END  {tabs--;}
        
/* Definicion de expresiones condicionales */
CONDITION       : BOOLEAN_VAL AND CONDITION {$<string>$ = malloc(strlen($<string>1)  + strlen($<string>3) + 10); sprintf($<string>$, "%s and %s", $<string>1, $<string>3);  free($<string>1); free($<string>3);}
                | BOOLEAN_VAL OR CONDITION {$<string>$ = malloc(strlen($<string>1) + strlen($<string>3) + 10); sprintf($<string>$, "%s or %s", $<string>1, $<string>3);  free($<string>1);  free($<string>3);}
                | BOOLEAN_VAL {$<string>$ = $<string>1;}

BOOLEAN_VAL     : ECUATION COMPARATOR ECUATION {$<string>$ = malloc(strlen($<string>1) +strlen($<string>3) + strlen($<string>2) + 100 + 3); sprintf($<string>$, "%s %s %s", $<string>1, $<string>2, $<string>3);  free($<string>2);} 
                | '(' BOOLEAN_VAL ')' {$<string>$ = malloc(strlen($<string>2) + 3); sprintf($<string>$, "(%s)", $<string>2); free($<string>2);}


FIGURE_VAL      : FIGURE RED OPERATION OPERATION ARRAY {$<string>$ = malloc(strlen($<string>3) + strlen($<string>4) +strlen($<string>5) + 500); sprintf($<string>$, "go.Figure(data=[go.Mesh3d(x=((%s)), y=((%s)),z=((%s)),opacity=0.5,color='rgba(244,22,100,0.6)')])", $<string>3,$<string>4,$<string>5);}
                | TRACE FIGURE {$<string>$ = malloc(100); sprintf($<string>$, "go.Figure()");}

SET_LAYOUT      : LAYOUT VARIABLE ARRAY ARRAY ARRAY ';' {$<string>$ = malloc(strlen($<string>5) + strlen($<string>2) + strlen($<string>3) + strlen($<string>4) + 500); sprintf($<string>$, "%s.update_layout(scene = dict(xaxis = dict(nticks=4, range=%s,),yaxis = dict(nticks=4, range=%s,),zaxis = dict(nticks=4, range=%s,),),width=700,margin=dict(r=20, l=10, b=10, t=10))",$<string>2, $<string>3,$<string>4,$<string>5);}

/* Estado que incluye la definicion de la directiva encargada de realizar el plot de los graficos */
DRAW_FIGURE     : DRAW VARIABLE ';' {$<string>$ = malloc(strlen($<string>2) + 7); sprintf($<string>$, "%s.show()", $<string>2);}

/*
 * Estado donde se define la directiva encargada de graficar funciones
 * Se pueden plotear graficos de dispercion poblacional y graficos de funciones
 * en R2 donde la iteracion se realiza en una variable, es decir, funciones del
 * estilo F(x) = <func>
 */
ADD_TRACE       : VARIABLE TRACE STRING RED VARIABLE VARIABLE';' {$<string>$ = malloc(strlen($<string>1) + strlen($<string>4)+500); sprintf($<string>$, "%s.add_trace(go.Scatter(x=%s, y=%s, name=%s,line = dict(color='red', width=4, dash='dash')))",$<string>1,$<string>5,$<string>6,$<string>3 ); free($<string>1); free($<string>5); free($<string>6); }
                | VARIABLE TRACE STRING RED ARRAY ARRAY ';'  {$<string>$ = malloc(strlen($<string>1) + strlen($<string>4)+500); sprintf($<string>$, "%s.add_trace(go.Scatter(x=%s, y=%s, name=%s,line = dict(color='red', width=4, dash='dash')))"  ,$<string>1,$<string>5,$<string>6,$<string>3);free($<string>1); free($<string>5); free($<string>6);}
                | VARIABLE TRACE STRING RED ARRAY VARIABLE';' {$<string>$ = malloc(strlen($<string>1) + strlen($<string>4)+500); sprintf($<string>$, "%s.add_trace(go.Scatter(x=%s, y=%s, name=%s,line = dict(color='red', width=4, dash='dash')))",$<string>1,$<string>5,$<string>6,$<string>3 ); free($<string>1); free($<string>5); free($<string>6);}
                | VARIABLE TRACE STRING RED VARIABLE ARRAY ';'  {$<string>$ = malloc(strlen($<string>1) + strlen($<string>4)+500); sprintf($<string>$, "%s.add_trace(go.Scatter(x=%s, y=%s, name=%s,line = dict(color='red', width=4, dash='dash')))",$<string>1,$<string>5,$<string>6,$<string>3) ;free($<string>1); free($<string>5); free($<string>6);}
                | VARIABLE TRACE RED VARIABLE_DECLARATION STRING REDUCED_EXP REDUCED_EXP REDUCED_EXP ';'{char * aux = remove_last_char($<string>4);$<string>$ = malloc(strlen($<string>1) + strlen($<string>4) + strlen($<string>5) + 1000);sprintf($<string>$,"%s.add_trace(go.Scatter(x=list(range(%d,%d,%d)), y=[eval(%s, {\"%s\":%s}) for %s in range(%d,%d,%d)], name=%s,line = dict(color='red', width=4, dash='dash')))",  $<string>1, (int)$<decimal>6,(int)$<decimal>7,(int)$<decimal>8,$<string>5, aux,$<string>4,$<string>4, (int)$<decimal>6,(int)$<decimal>7,(int)$<decimal>8,$<string>5);free($<string>1); free($<string>4);}

                | VARIABLE TRACE STRING YELLOW VARIABLE VARIABLE';' {$<string>$ = malloc(strlen($<string>1) + strlen($<string>4)+500); sprintf($<string>$, "%s.add_trace(go.Scatter(x=%s, y=%s, name=%s,line = dict(color='yellow', width=4, dash='dash')))",$<string>1,$<string>5,$<string>6,$<string>3 ); free($<string>1); free($<string>5); free($<string>6);}
                | VARIABLE TRACE STRING YELLOW ARRAY ARRAY ';'  {$<string>$ = malloc(strlen($<string>1) + strlen($<string>4)+500); sprintf($<string>$, "%s.add_trace(go.Scatter(x=%s, y=%s, name=%s,line = dict(color='yellow', width=4, dash='dash')))"  ,$<string>1,$<string>5,$<string>6,$<string>3);free($<string>1); free($<string>5); free($<string>6);}
                | VARIABLE TRACE STRING YELLOW ARRAY VARIABLE';' {$<string>$ = malloc(strlen($<string>1) + strlen($<string>4)+500); sprintf($<string>$, "%s.add_trace(go.Scatter(x=%s, y=%s, name=%s,line = dict(color='yellow', width=4, dash='dash')))",$<string>1,$<string>5,$<string>6,$<string>3 ); free($<string>1); free($<string>5); free($<string>6);}
                | VARIABLE TRACE STRING YELLOW VARIABLE ARRAY ';'  {$<string>$ = malloc(strlen($<string>1) + strlen($<string>4)+500); sprintf($<string>$, "%s.add_trace(go.Scatter(x=%s, y=%s, name=%s,line = dict(color='yellow', width=4, dash='dash')))",$<string>1,$<string>5,$<string>6,$<string>3) ;free($<string>1); free($<string>5); free($<string>6);}
                | VARIABLE TRACE YELLOW VARIABLE_DECLARATION STRING REDUCED_EXP REDUCED_EXP REDUCED_EXP ';' {char * aux = remove_last_char($<string>4);$<string>$ = malloc(strlen($<string>1) + strlen($<string>4) + strlen($<string>5) + 1000);sprintf($<string>$,"%s.add_trace(go.Scatter(x=list(range(%d,%d,%d)), y=[eval(%s, {\"%s\":%s}) for %s in range(%d,%d,%d)], name=%s,line = dict(color='yellow', width=4, dash='dash')))",  $<string>1, (int)$<decimal>6,(int)$<decimal>7,(int)$<decimal>8,$<string>5, aux,$<string>4,$<string>4, (int)$<decimal>6,(int)$<decimal>7,(int)$<decimal>8,$<string>5);free($<string>1); free($<string>4);}

                | VARIABLE TRACE STRING BLUE VARIABLE VARIABLE';' {$<string>$ = malloc(strlen($<string>1) + strlen($<string>4)+500); sprintf($<string>$, "%s.add_trace(go.Scatter(x=%s, y=%s, name=%s,line = dict(color='blue', width=4, dash='dash')))",$<string>1,$<string>5,$<string>6,$<string>3 ); free($<string>1); free($<string>5); free($<string>6);}
                | VARIABLE TRACE STRING BLUE ARRAY ARRAY ';'  {$<string>$ = malloc(strlen($<string>1) + strlen($<string>4)+500); sprintf($<string>$, "%s.add_trace(go.Scatter(x=%s, y=%s, name=%s,line = dict(color='blue', width=4, dash='dash')))"  ,$<string>1,$<string>5,$<string>6,$<string>3);free($<string>1); free($<string>5); free($<string>6);}
                | VARIABLE TRACE STRING BLUE ARRAY VARIABLE';' {$<string>$ = malloc(strlen($<string>1) + strlen($<string>4)+500); sprintf($<string>$, "%s.add_trace(go.Scatter(x=%s, y=%s, name=%s,line = dict(color='blue', width=4, dash='dash')))",$<string>1,$<string>5,$<string>6,$<string>3 );free($<string>1); free($<string>5); free($<string>6); }
                | VARIABLE TRACE STRING BLUE VARIABLE ARRAY ';'  {$<string>$ = malloc(strlen($<string>1) + strlen($<string>4)+500); sprintf($<string>$, "%s.add_trace(go.Scatter(x=%s, y=%s, name=%s,line = dict(color='blue', width=4, dash='dash')))",$<string>1,$<string>5,$<string>6,$<string>3) ;free($<string>1); free($<string>5); free($<string>6);}
                | VARIABLE TRACE BLUE VARIABLE_DECLARATION STRING REDUCED_EXP REDUCED_EXP REDUCED_EXP ';' {char * aux = remove_last_char($<string>4);$<string>$ = malloc(strlen($<string>1) + strlen($<string>4) + strlen($<string>5) + 1000);sprintf($<string>$,"%s.add_trace(go.Scatter(x=list(range(%d,%d,%d)), y=[eval(%s, {\"%s\":%s}) for %s in range(%d,%d,%d)], name=%s,line = dict(color='blue', width=4, dash='dash')))",  $<string>1, (int)$<decimal>6,(int)$<decimal>7,(int)$<decimal>8,$<string>5, aux,$<string>4,$<string>4, (int)$<decimal>6,(int)$<decimal>7,(int)$<decimal>8,$<string>5);free($<string>1);free($<string>4);}

                | VARIABLE TRACE STRING GREEN VARIABLE VARIABLE';' {$<string>$ = malloc(strlen($<string>1) + strlen($<string>4)+500); sprintf($<string>$, "%s.add_trace(go.Scatter(x=%s, y=%s, name=%s,line = dict(color='green', width=4, dash='dash')))",$<string>1,$<string>5,$<string>6,$<string>3 ); free($<string>1); free($<string>5); free($<string>6);}
                | VARIABLE TRACE STRING GREEN ARRAY ARRAY ';'  {$<string>$ = malloc(strlen($<string>1) + strlen($<string>4)+500); sprintf($<string>$, "%s.add_trace(go.Scatter(x=%s, y=%s, name=%s,line = dict(color='green', width=4, dash='dash')))"  ,$<string>1,$<string>5,$<string>6,$<string>3);free($<string>1); free($<string>5); free($<string>6);}
                | VARIABLE TRACE STRING GREEN ARRAY VARIABLE';' {$<string>$ = malloc(strlen($<string>1) + strlen($<string>4)+500); sprintf($<string>$, "%s.add_trace(go.Scatter(x=%s, y=%s, name=%s,line = dict(color='green', width=4, dash='dash')))",$<string>1,$<string>5,$<string>6,$<string>3 ); free($<string>1); free($<string>5); free($<string>6);}
                | VARIABLE TRACE STRING GREEN VARIABLE ARRAY ';'  {$<string>$ = malloc(strlen($<string>1) + strlen($<string>4)+500); sprintf($<string>$, "%s.add_trace(go.Scatter(x=%s, y=%s, name=%s,line = dict(color='green', width=4, dash='dash')))",$<string>1,$<string>5,$<string>6,$<string>3) ;free($<string>1); free($<string>5); free($<string>6);}
                | VARIABLE TRACE GREEN VARIABLE_DECLARATION STRING REDUCED_EXP REDUCED_EXP REDUCED_EXP ';' {char * aux = remove_last_char($<string>4);$<string>$ = malloc(strlen($<string>1) + strlen($<string>4) + strlen($<string>5) + 1000);sprintf($<string>$,"%s.add_trace(go.Scatter(x=list(range(%d,%d,%d)), y=[eval(%s, {\"%s\":%s}) for %s in range(%d,%d,%d)], name=%s,line = dict(color='green', width=4, dash='dash')))", $<string>1, (int)$<decimal>6,(int)$<decimal>7,(int)$<decimal>8,$<string>5, aux,$<string>4,$<string>4, (int)$<decimal>6,(int)$<decimal>7,(int)$<decimal>8,$<string>5);free($<string>1); free($<string>4); }

/*
 * Estado donde se define la funcion evaluate la cual nos permite pasarle mediante un string
 * una funciona a evaluar seguido de los valores que cada variable en ese string debe tomar
 * y de esta forma poder obtener un resultado numerico. Formato:
 *      EVAL "<func>" {<variables>}
 *      <variables> = "<variable>" : [<value>|<operation>], ... ,"<variable>" : [<value>|<operation>]
 */
EVALUATE          : EVAL STRING VARIABLES {$<string>$ = malloc(strlen($<string>2)+strlen($<string>3)+10);sprintf($<string>$, "eval(%s,%s)",$<string>2, $<string>3);}
VARIABLES         : '{' VARIABLES_TO_EVAL '}'  {$<string>$ = malloc(strlen($<string>2)+2);sprintf($<string>$, "{%s}",$<string>2);}
VARIABLES_TO_EVAL : VARIABLE_DECLARATION ':' OPERATION ',' VARIABLES_TO_EVAL {$<string>$ = malloc(strlen($<string>1)+1+strlen($<string>3)+1+strlen($<string>5)+2);sprintf($<string>$, "\"%s\":%s,%s",remove_last_char($<string>1),$<string>3,$<string>5);}
                  | VARIABLE_DECLARATION ':' OPERATION {$<string>$ = malloc(strlen($<string>1)+1+strlen($<string>3)+2);sprintf($<string>$, "\"%s\":%s",remove_last_char($<string>1),$<string>3);}

%%

int yywrap()
{
    return 1;
}

int main(int argc, char * argv[]) {
    
    /* Parametro extra donde se guardara el codigo python. */
    char * result;
    
    yydebug=1;
    /*
     * Funcion que inicializa el hashmap, recibe:
     * 1. Los tipos de dato a utilizar definidos mas arriba.
     * 2. Funcion de hashing para la key, en nuestro caso esta ya vino implementada
     *    por la libreria. La misma es case sensitive y fue obtenida de un libro para 
     *    garantizar la eficiencia (https://en.wikipedia.org/wiki/Jenkins_hash_function).
     * 3. Funcion que se utiliza para realizar las comparaciones entre las keys, se
     *    utilizo la funcion que viene en la libreria string.h conocida como "strcmp"
     *    la cual compara 2 strings caracter a caracter y retorna su diferencia.
     */
    hashmap_init(&map, hashmap_hash_string, strcmp);

    /*
     * Si se agrego codigo entre los delimitadores START y FINISH 
     * tenemos que importar estas dependencias.
     */
    yyparse(&result);

    /* Si hay codigo entre START y FINISH */
    if(result != NULL){

        /* Creamos el archivo python */
        FILE * output = fopen("out.py", "w");

        /* 
         * Agregamos las librerias que vamos a utilizar y 
         * el codigo python resultante
         */
        fprintf(output, "import numpy as np \n"
                        "import sys\n"
                        "import plotly.graph_objects as go\n" 
                        "np.random.seed(1)\n"
                        "N = 70\n"
                        "\n%s",result);
        
        // printf("%s", result);

        /* Cerramos el archivo */
        fclose(output);

    }

    /* 
     * Limpiamos el contenido del hashmap esta macro ya es provista por la libreria de hashmap
     * Los datos son asignados a var y luego son liberados, es necesarios pasarle el hashmap en si
     * a la macro para que pueda iterar por el mismo.
     */
    hashmap_foreach_data(var, &map) {
        free(var);
    }
    /*
     * Una vez liberados los datos del hashmap utilizamos otra macro la cual
     * se encarga de liberar el hashmap en si y toda la memoria asociada al mismo.
     */
    hashmap_cleanup(&map);

    /* Destruimos el parser */
    yylex_destroy();

    /* 
     * LLegado a este punto toda la memoria alocada fue liberada  
     * retornamos 0 indicando que no hubo problemas
     */
    
    return 0;
}

double checkZero(double d){
    if(d == 0.0){
        char ** code;
        yyerror(code, "Can't divide by 0!");
    }
    return d;
}

/* Funcion a la cual se llama si hubo un problema, indica en que linea del codigo ocurrio */
void yyerror (char ** result, const char *s){
    extern int yylineno;
    fprintf(stderr,"Error at line %d: %s ",yylineno,s);
    exit(1);
}

/* Funcion que concatena strings */
char * concatenate_code(char * string1, char* string2){
    char * resp;
    if(string2 != NULL) {       
        resp = malloc(strlen(string1) + strlen(string2) + 2); 
        sprintf(resp, "%s\n%s",string1,string2);
 
        free(string2);

        fflush( stdout );
       }
    else{
        resp = malloc(strlen(string1) + 2); 
        sprintf(resp, "%s\n", string1);  
    }
    free(string1);
    return resp;
}

/* Funcion para agregar los tabs al momento de transformar el codigo a Python */
void addTabs(char* s, int n){ 
    for (int i = 0; i < n; i++){
        memmove(s + 1, s, strlen(s) + 1);
        memcpy(s, "\t", 1);
    }
}

/* Declarar la variable y agregarla en el hashmap */
char * declareVar(char * v, char * op, int flag){
    
    /* Respuesta */
    char * resp;

    struct variable *aux = malloc(sizeof(*aux));
    aux->is_cte = flag;
    /* 
     * Esta macro agrega la nueva entrada, en nuestro caso
     * la variable (solo el nombre) al hashmap. Si la variable
     * existe no lo agrega.
     */
    int ret = hashmap_put(&map, v, aux);
    
    /* Lo pasamos a codigo python */
    if(op != NULL){
        resp = malloc( strlen(v) + 3 + strlen(op));
        sprintf(resp,"%s = %s", v, op); 
        free(op); 
    }else{
        resp = malloc( strlen(v) + 6);
        sprintf(resp,"%s=None", v);
    }
    return resp;
}

void checkIfConst(char*v){
    struct variable *aux = hashmap_get(&map,v);

    if( aux != NULL && aux->is_cte == 1){
        char**code;
        yyerror(code, "Cannot step on constants values!");
    }
}

/* Funcion que chequea si una variable fue declarada */
char * checkIfVarExists(char * v){
    
    /* Respuesta */
    char * resp;

    /* Esta macro retorna un valor del hashmap
     * si existe devuelve un puntero a la informacion del hashmap
     * sino devuelve NULL 
     */
    if(hashmap_get(&map, v) == NULL){
        /* Si entro aca entonces la variable no esta declarada */
        char ** code;
        char * error = malloc(30 + strlen(v));
        char * aux = remove_last_char(v);
        sprintf(error,"Undefined variable %s\n",aux);
        yyerror(code,error);
    }

    return resp;
}

/*
 * Remueve el ultimo caracter de la variable
 * Esto es porque internamente se les concatena 
 * un '_' al final para evitar que se usen 
 * palabras reservadas como variables
 */ 
char * remove_last_char(char * str){
    char * aux = malloc(strlen(str));
    strncpy(aux, str, strlen(str)-1);
    return aux;
}

