CC = gcc
YACC = yacc
LEX = lex

#files
NAME_YACC = yacc.y
NAME_LEX = grammar.l
HASHMAP = Utils/hashmap
LEX_OUT = lex.yy
YACC_OUT = y.tab
OUTPUT = compiler
OUT_FILES_C = $(LEX_OUT).c $(YACC_OUT).c
OUT_FILES_H = $(YACC_OUT).h
OUT_FILES_O = $(OUT_FILES_C:.c=.o) $(HASHMAP).o 
OUT_FILES = $(OUT_FILES_C) $(OUT_FILES_H) $(OUT_FILES_O) $(OUTPUT) mateCompiler.out

#flags
YACC_FLAGS = -d
CC_FLAGS = -lm -ly -std=c99 -Wall -pedantic




all: $(OUTPUT)

$(OUTPUT): $(OUT_FILES_O)
	$(CC) -o $(OUTPUT) $(OUT_FILES_O) $(CC_FLAGS)

$(YACC_OUT).o: $(YACC_OUT).c
	$(CC) -c $(YACC_OUT).c

$(HASHMAP).o: $(HASHMAP).c
	$(CC) -c $(HASHMAP).c -o $(HASHMAP).o


$(LEX_OUT).o: $(LEX_OUT).c
	$(CC) -c $(LEX_OUT).c

$(LEX_OUT).c: $(YACC_OUT).h
	$(LEX)  $(NAME_LEX)

$(YACC_OUT).h: $(YACC_OUT).c

$(YACC_OUT).c:
	$(YACC) $(YACC_FLAGS) $(NAME_YACC)

clean:
	rm -f $(OUT_FILES)